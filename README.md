# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://bitbucket.org/Lukaznid/stroboskop/overview
cd stroboskop
```

Naloga 6.2.3:
https://bitbucket.org/Lukaznid/stroboskop/commits/d4e6297273903e1643935f1f5c8f6899cba71a6d?at=master

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Lukaznid/stroboskop/commits/134141e53daedf7da5b8ca5c3bc6ac7c4713a2af?at=izgled

Naloga 6.3.2:
https://bitbucket.org/Lukaznid/stroboskop/commits/fa457c5faf77ca6915e3a0187cd2bc717cc482c1?at=izgled

Naloga 6.3.3:
https://bitbucket.org/Lukaznid/stroboskop/commits/fd573a0e8ef22cc8881b050cd8648d29d6afa8d2?at=izgled

Naloga 6.3.4:
https://bitbucket.org/Lukaznid/stroboskop/commits/de72085392708bd5d657a509564f2bbfe1f39de0?at=izgled

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Lukaznid/stroboskop/commits/197e2d3f954663fe601d0845273cc1aa5a3a4837?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/Lukaznid/stroboskop/commits/41163c94bf0382414bfae0f0d2dae5949f047e3c?at=dinamika

Naloga 6.4.3:
https://bitbucket.org/Lukaznid/stroboskop/commits/42c1c0067c5c220dfaf605c077badd23a216629f?at=dinamika

Naloga 6.4.4:
https://bitbucket.org/Lukaznid/stroboskop/commits/4c97c18771fa9071a730e1c95f80415e8a26e0d6?at=dinamika